from .request import Request, threads
import gitlab
import os
import time

start_time = time.time()

pa_token = os.environ["pa_token"]
pipeline_trigger = os.environ["pipeline_trigger"]
group = "ZeroNet-alpha-management"

gl = gitlab.Gitlab("https://gitlab.com", private_token=pa_token)

group_resource = gl.groups.get(group)

project = gl.projects.get(f"{group}/ZeroNet")

members = [member.username for member in project.members.list(all=True)]
creator = gl.users.get(project.creator_id).username
if creator not in members:
    members.append(creator)

# Process
while time.time() - start_time < 30 * 60:  # 30 min
    for issue in group_resource.issues.list():
        issue_project = gl.projects.get(issue.project_id)
        issue = issue_project.issues.get(issue.iid)
        project_name = issue_project.name
        context = "report" if project_name.startswith("Report-") else "zeronet"
        for discussion in issue.discussions.list():
            # Was this discussion answered already?
            if any(note["author"]["username"] == "zeronet-bot" for note in discussion.attributes["notes"]):
                continue
            # Is this discussion a bot request?
            note = discussion.attributes["notes"][0]
            if "@zeronet-bot" in note["body"]:
                for line in note["body"].split("\n"):
                    if "/" in line:
                        request = line.split("/", 1)[1].strip()
                        response = Request(gl, group, note["author"]["username"], members, context, project, issue_project, pa_token, issue).handle(request)
                        try:
                            discussion.notes.create({
                                "body": response
                            })
                        except Exception:
                            pass
    time.sleep(5)

# Wait for all threads
for thread in threads:
    thread.join()

# Restart CI
gl.projects.get(f"{group}/bot").trigger_pipeline("master", pipeline_trigger)