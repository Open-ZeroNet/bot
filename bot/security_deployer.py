from threading import Thread
import gitlab
import subprocess
import sys
import os
import re

if sys.platform == "win32":
    SH_C = ("cmd", "/c")
else:
    SH_C = ("sh", "-c")


class SecurityDeployer(Thread):
    def __init__(self, pa_token, issue_iid, report_path, group):
        super().__init__()
        self.pa_token = pa_token
        self.issue_iid = issue_iid
        self.report_path = report_path
        self.group = group


    def run(self):
        # Create Gitlab object
        self.gl = gitlab.Gitlab("https://gitlab.com", private_token=self.pa_token)
        self.report_project = self.gl.projects.get(self.report_path)
        issue = self.report_project.issues.get(1)

        # Fetch
        def run(*args, capture_output=False, **kwargs):
            if not capture_output:
                kwargs["stderr"] = subprocess.STDOUT
                kwargs["stdout"] = subprocess.DEVNULL
            return subprocess.run(
                args,
                cwd=f"forks/{self.report_path}",
                capture_output=capture_output,
                **kwargs
            )
        os.makedirs(f"forks/{self.report_path}", exist_ok=True)
        run("git", "init")
        report_url = f"https://zeronet-bot:{self.pa_token}@gitlab.com/{self.report_path}.git"
        run("git", "remote", "add", "origin", report_url)
        run("git", "fetch", "origin")

        # Add upstream
        upstream_url = f"https://zeronet-bot:{self.pa_token}@gitlab.com/{self.group}/ZeroNet.git"
        run("git", "remote", "add", "upstream", upstream_url)

        # Merge
        run("git", "fetch", "upstream")
        run("git", "reset", "--hard", "upstream/py3")
        run("git", "checkout", "--detach")
        run("git", "branch", "-f", "py3")
        run("git", "checkout", "py3")
        merge_result = run(*SH_C, "git merge origin/fix --no-ff --no-edit 2>&1", capture_output=True)
        if merge_result.returncode != 0:
            # Failed to merge, probably some merge conflicts?
            run("git", "merge", "--abort")
            issue.discussions.create({
                "body": f"""
🛑 Failed to merge fix. The deploy was aborted. Please fix the merge conflicts.
`git merge` result follows:

```
{merge_result.stdout.strip().decode()}
```

```
{merge_result.stderr.strip().decode()}
```
                """
            })
            return

        # Increment version & revision
        with open(f"forks/{self.report_path}/src/Config.py") as f:
            config = f.read()
        # Parse & increment
        old_version = re.search("self\\.version = \"(.*)\"", config).group(1)
        old_rev = re.search("self\\.rev = (\d+)", config).group(1)
        patch = old_version.split(".")[-1]
        patch = str(int(patch) + 1)
        version = ".".join(old_version.split(".")[:-1] + [patch])
        rev = str(int(old_rev) + 1)
        # Update
        config = config.replace(
            f"self.version = \"{old_version}\"",
            f"self.version = \"{version}\""
        )
        config = config.replace(
            f"self.rev = {old_rev}",
            f"self.rev = {rev}"
        )
        with open(f"forks/{self.report_path}/src/Config.py", "w") as f:
            f.write(config)
        run("git", "commit", "-a", "-m", "Increment version & revision")

        # Push
        push_result = run("git", "push", "upstream", "py3", capture_output=True)
        if push_result.returncode != 0:
            # Failed to push
            issue.discussions.create({
                "body": f"""
🛑 Failed to push fix. The deploy was aborted. `git push` result follows:

```
{push_result.stdout.strip().decode()}
```

```
{push_result.stderr.strip().decode()}
```
                """
            })
            return

        # Complete
        issue.discussions.create({
            "body": f"🎉 The fix was deployed successfully"
        })