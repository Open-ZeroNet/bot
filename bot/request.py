import gitlab
import inspect
import os
import subprocess
import datetime
from .security_deployer import SecurityDeployer

threads = []



class Request:
    commands = {
        "private-report": ("privateReport", ("zeronet",)),
        "invite-to-report": ("inviteToReport", ("report",)),
        "delete-report": ("deleteReport", ("report",)),
        "publish-report": ("publishReport", ("report",)),
        "set-fix": ("setFix", ("report",)),
        "accept-fix": ("acceptFix", ("report",))
    }

    def __init__(self, gl, group, author, members, context, project, issue_project, pa_token, issue):
        self.gl = gl
        self.group = group
        self.author = author
        self.members = members
        self.context = context
        self.project = project
        self.issue_project = issue_project
        self.pa_token = pa_token
        self.issue = issue


    def handle(self, request):
        args = request.split()
        cmd = args.pop(0)
        if cmd in self.commands:
            name, contexts = self.commands[cmd]
            # Check context
            if self.context not in contexts:
                return f"🛑 Command `/{cmd}` cannot be used in `{self.context}` context"
            f = getattr(self, name)
            # Check argument count
            params = inspect.signature(f).parameters
            is_at_least = any(param.kind == inspect.Parameter.VAR_POSITIONAL for param in params.values())
            cnt_params = len(params) - int(is_at_least)
            if len(args) < cnt_params or (len(args) > cnt_params and not is_at_least):
                signature = " ".join(
                    ("*" if param.kind == inspect.Parameter.VAR_POSITIONAL else "") +
                    param.name
                    for param in params
                )
                return f"🛑 Wrong count of arguments for `/{cmd}`. Signature: `/{cmd} {signature}`"
            # Everything is ok
            return f(*args)
        else:
            return f"🛑 Don't know how to make `/{cmd}`. Stop."


    def getAuthorAccessLevel(self):
        members = [
            (member.username, member.access_level)
            for member
            in self.issue_project.members.list(all=True)
        ]
        members += [
            (member, gitlab.MAINTAINER_ACCESS)
            for member
            in self.members
        ]
        levels = [level for member, level in members if member == self.author]
        if levels == []:
            return 0
        return levels[0]


    def privateReport(self):
        # Create a new repository
        group = self.gl.groups.get(self.group)
        project_names = [project.name for project in group.projects.list()]
        report_id = max(
            [0] + [
                int(name.replace("Report-", ""))
                for name in project_names
                if name.startswith("Report-")
            ]
        ) + 1
        project = self.gl.projects.create({
            "name": f"Report-{report_id}",
            "namespace_id": group.id
        })
        # Create an issue
        issue = project.issues.create({
            "title": f"Private report #{report_id}",
            "description": f"This report was created by @{self.author}"
        })
        # Invite the author
        try:
            project.members.create({
                "user_id": self.gl.users.list(username=self.author)[0].id,
                "access_level": gitlab.REPORTER_ACCESS
            })
        except Exception:
            # Why are we catching something? Because Gitlab fails on adding a
            # user that's has inherited the permission from the main group
            pass
        # Ping members
        member_pings = " ".join(f"@{member}" for member in self.members)
        issue.discussions.create({
            "body": f"""
Hello, @{self.author}! Thanks for opening a private report. Private reports are
meant to be used for vulnerability reports and other critical issues.

In case it's your first time opening a private issue, let me explain what you
can do with it a bit:

- Invite other people to this report (for example, if you found the
  vulnerability with a team) by commenting
  `@zeronet-bot /invite-to-report @user1 @user2 ...`;
- Delete the report if you come to conclusion that it shouldn't be private or
  that you were dumb with `@zeronet-bot /delete-report`;
- Upload a vulnerability fix with
  `@zeronet-bot /set-fix yourusername/reponame branchname`;
- If you are a ZeroNet maintainer, you can accept the promoted fix with
  `@zeronet-bot /accept-fix`. Once half of maintainers or a single owner approve
  the fix, the fix will be merged into ZeroNet automatically;
- If you are a ZeroNet owner, you can make the report public with
  `@zeronet-bot /publish-report`.

If you have any problems using private issues, please create an issue in the
main ZeroNet repository.

*Pinging maintainers: {member_pings}*
            """
        })
        # Actually create the report
        return f"🎉 The report was created for you at [{self.group}/Report-{report_id}](https://gitlab.com/{self.group}/Report-{report_id}/issues/1)"


    def inviteToReport(self, *users):
        if self.getAuthorAccessLevel() < gitlab.GUEST_ACCESS:
            return "🛑 You are not allowed to invite anyone"

        users = [user.lstrip("@") for user in users]
        success = []
        errors = []
        for user in users:
            try:
                user_id = self.gl.users.list(username=user)[0].id
            except Exception:
                errors.append(f"- User @{user} does not exist")
                continue
            try:
                self.issue_project.members.create({
                    "user_id": user_id,
                    "access_level": gitlab.GUEST_ACCESS
                })
            except Exception:
                errors.append(f"- @{user} is already invited")
                continue
            success.append(f"@{user}")
        if success == [] and errors == []:
            return "🎉 Asked for nothing, did nothing"
        elif success == []:
            return "🛑 Failed to invite users:\n\n" + "\n".join(errors)
        elif errors == []:
            if len(success) == 1:
                return "🎉 " + success[0] + " was invited"
            else:
                return "🎉 " + ", ".join(success) + " were invited"
        else:
            if len(success) == 1:
                return "🛑 " + success[0] + " was invited, but inviting others failed:\n\n" + "\n".join(errors)
            else:
                return "🛑 " + ", ".join(success) + " were invited, but inviting others failed:\n\n" + "\n".join(errors)


    def deleteReport(self):
        if self.getAuthorAccessLevel() < gitlab.REPORTER_ACCESS:
            return "🛑 Only report author and ZeroNet maintainers can delete this report"
        report_id = self.issue_project.name.replace("Report-", "")
        self.project.issues.get(1).discussions.create({
            "body": f"Private report {report_id} was deleted"
        })
        self.issue_project.delete()
        return ""


    def publishReport(self):
        if self.getAuthorAccessLevel() < gitlab.GUEST_ACCESS:
            return "🛑 You are not allowed to publish the report"
        report_id = self.issue_project.name.replace("Report-", "")
        self.project.issues.get(1).discussions.create({
            "body": f"[Private report {report_id}](https://gitlab.com/{self.issue_project.path_with_namespace}/issues/1) was published"
        })
        self.issue_project.visibility = "public"
        self.issue_project.save()
        return "🎉 The report is now public"


    def setFix(self, repository, branch):
        if self.getAuthorAccessLevel() < gitlab.GUEST_ACCESS:
            return "🛑 You are not allowed to set the fix"
        report_id = self.issue_project.name.replace("Report-", "")
        # Check project
        try:
            project = self.gl.projects.get(repository)
        except Exception:
            return f"""
🛑 Could not access project {repository}. Make sure that the name is correct and
@zeronet-bot has the correct permissions.
            """
        path = project.path_with_namespace
        url = f"https://zeronet-bot:{self.pa_token}@gitlab.com/{path}.git"
        report_url = f"https://zeronet-bot:{self.pa_token}@gitlab.com/{self.issue_project.path_with_namespace}.git"
        # Download it
        def run(*args, capture_output=False, **kwargs):
            if not capture_output:
                kwargs["stderr"] = subprocess.STDOUT
                kwargs["stdout"] = subprocess.DEVNULL
            return subprocess.run(
                args,
                cwd=f"forks/{path}",
                capture_output=capture_output,
                **kwargs
            )
        os.makedirs(f"forks/{path}", exist_ok=True)
        run("git", "init")
        run("git", "remote", "add", "origin", url)
        run("git", "remote", "add", f"report-{report_id}", report_url)
        if run("git", "fetch", "origin").returncode != 0:
            return f"""
🛑 Could not download project {repository}. Make sure that the name is correct
and @zeronet-bot has the correct permissions.
            """
        # Check the branch
        commit = run("git", "rev-parse", f"refs/remotes/origin/{branch}", capture_output=True)
        if commit.returncode != 0:
            return f"🛑 Branch {branch} does not exist on {repository}"
        commit = commit.stdout.strip().decode()
        # Push the fix
        run("git", "push", "--force", f"report-{report_id}", f"origin/{branch}:refs/heads/fix")
        fix_url = f"https://gitlab.com/{self.issue_project.path_with_namespace}/tree/fix"
        return f"""
🎉 Uploaded [fix]({fix_url}) at commit {commit}. **The voting will start after a
minute cooldown.** Use this time to check the fix attentively.

If you are a ZeroNet maintainer and you support the fix, please use
`@zeronet-bot /accept-fix` to accept the fix. Once at least half maintainers or
at least one owner accept the fix, the following actions will take place:

- The fix will be merged into ZeroNet core;
- The version and the revision will be incremented;
- The code will be uploaded to ZeroUpdate;
- Users will be faced with a warning.
        """


    def acceptFix(self):
        # Check cooldown
        fix_timestamp = None
        accepted_by = []
        deploy_started = False
        for discussion in self.issue.discussions.list():
            for note in discussion.attributes["notes"]:
                if note["author"]["username"] == "zeronet-bot":
                    if "The voting will start" in note["body"]:
                        fix_timestamp = datetime.datetime.fromisoformat(note["created_at"].replace("Z", "+00:00")).timestamp()
                        accepted_by.clear()
                    elif "Your acceptance was counted" in note["body"]:
                        accepted_by.append(note["author"]["username"])
                        if "The deploy has just started" in note["body"]:
                            deploy_started = True
                    elif "Failed to merge fix" in note["body"] or "Failed to push fix" in note["body"]:
                        deploy_started = False
        if deploy_started:
            return "🛑 The deploy has already started"
        if fix_timestamp is None:
            return "🛑 No fix was set yet. Use `@zeronet-bot /set-fix yourusername/yourrepository branchname`."
        time_since_set_fix = datetime.datetime.now().timestamp() - fix_timestamp
        if time_since_set_fix < 60:
            return "🛑 One minute cooldown is active"
        # Check access level
        access_level = self.getAuthorAccessLevel()
        if access_level < gitlab.MAINTAINER_ACCESS:
            return "🛑 Only ZeroNet maintainers can accept a fix"
        # Actually accept
        accepted_by.append(self.author)
        accepted_by = set(accepted_by)
        is_accepted = (
            access_level == gitlab.OWNER_ACCESS or  # Accepted by owner
            len(accepted_by) >= (len(self.members) + 1) // 2  # Accepted by a majority of maintainers
        )
        if is_accepted:
            thread = SecurityDeployer(self.pa_token, self.issue.iid, self.issue_project.path_with_namespace, self.group)
            thread.start()
            threads.append(thread)
            return "🎉 Your acceptance was counted. The deploy has just started."
        else:
            return "🎉 Your acceptance was counted. Waiting for other maintainers."